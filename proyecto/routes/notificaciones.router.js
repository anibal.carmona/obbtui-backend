'use strict';
var express = require('express'),
  notificacionesService = require('../services/notificaciones.service'),
  router = express.Router();

router.post('/', notificacionesService.getNotificaciones);

module.exports = router;