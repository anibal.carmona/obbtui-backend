'use strict';
var express = require('express'),
  novedadesService = require('../services/novedades.service'),
  router = express.Router();

router.post('/', novedadesService.getNovedades);

module.exports = router;